#!/bin/sh

DIR=~/.log/
TEMP=.temp
TEMPG=.tempg
TEMPF=.tempf
FMT=.xml
DIRIMAGE=~/.log/images/
BLAZE='Antoine'

function createDir(){
  curl -u $USER:$PASS  $DIRFTP'images/'$1'/' --ftp-create-dirs
  #curl -u $USER:$PASS -T $DIR'xml/test/' $DIRFTP'xml/'
}

function sendLog(){
  curl -u $USER:$PASS -T $DIR'xml/'$1$FMT $DIRFTP'xml/'
}
function sendImg(){
    IMAGE=$1
    HTTP=${IMAGE:0:4}
    EXTENS=${IMAGE: -4}
    FILE=$3

    if [ $HTTP = 'http' ]
    then
	curl $IMAGE -o $2$EXTENS
	curl -u $USER:$PASS -T $2$EXTENS $DIRFTP'images/'$FILE'/'

    else
	cp $IMAGE $2$EXTENS
	curl -u $USER:$PASS -T $2$EXTENS $DIRFTP'images/'$FILE'/'
    fi
    
    echo '<image>'$FILE'/'$DATE$EXTENS'</image>' >> $DIR$TEMP
}

function tmb(){
    
    DATE=`date +%Y-%m-%d__%H:%M:%S`
    if [ $1 = -h ]
    then
      echo 'TMB commands'
      echo '-c	    create a new log (.xml)'
      echo '-ls	    list'
      echo '-r	    read'
      echo '-m	    edit with vim'
      echo '-u	    upload'
      echo '-mu	    edit and upload'
      echo '-rm	    remove'

    elif [ $1 = -c ]
    then
	read -p 'title: ' titre
	read -p 'Description: ' describe
	read -p 'Enter tags: ' cate
	read -p 'Online (off or on) : ' online
	echo '<item>' >> $DIR'xml/'$2$FMT
	echo '	<meta>' >> $DIR'xml/'$2$FMT
	echo '	    <titre>'$titre'</titre>' >> $DIR'xml/'$2$FMT
	echo '	    <description>'$describe'</description>' >> $DIR'xml/'$2$FMT
	echo '	    <tags>'$cate'</tags>' >> $DIR'xml/'$2$FMT
	echo '	    <creationdate>'$DATE'</creationdate>' >> $DIR'xml/'$2$FMT
	echo '	    <modificationdate>'$DATE'</modificationdate>' >> $DIR'xml/'$2$FMT
	echo '	    <online>'$online'</online>' >> $DIR'xml/'$2$FMT
	echo '	</meta>' >> $DIR'xml/'$2$FMT
	echo '	<notes>' >> $DIR'xml/'$2$FMT
	echo '	</notes>' >> $DIR'xml/'$2$FMT
	echo '</item>' >> $DIR'xml/'$2$FMT
	
	
	mkdir $DIR'images/'$2'/'
	createDir $2
	sendLog $2
    
      
    elif [ $1 = -a ]
    then
	echo a
    elif [ $1 = -ls ]
    then
	ls -l $DIR'xml/' 
    elif [ $1 = -r ]
    then
	cat $DIR'xml/'$2$FMT
    elif [ $1 = -m ]
    then
	vim $DIR'xml/'$2$FMT
    elif [ $1 = -u ]
    then
	sendLog $2
    elif [ $1 = -mu ]
    then
	vim $DIR'xml/'$2$FMT
	sendLog $2

    elif  [ $1 = -rm ] 
    then
	rm $DIR'xml/'$2$FMT
    else

      # ADD POST
      FILE=$1
      # a modifier
      mv $DIR'xml/'$FILE$FMT $DIR'xml/.archive/.'$DATE'--'$FILE$FMT
      # rm $DIR'xml/'$FILE$FMT
      curl -u $USER:$PASS $DIRFTP'xml/'$FILE$FMT > $DIR'xml/'$FILE$FMT

      if [ $2 = -img ]
      then
	CONTENT=''
	ATRIMAGE=$2
	IMAGE=$3
      else
	CONTENT=$2
	ATRIMAGE=$3
	IMAGE=$4
      fi
	head -n -2 $DIR'xml/'$FILE$FMT > $DIR$TEMPG
	COUNT=$(sed -n 8p $DIR$TEMPG)
	IFS='>'
	set $COUNT
	COUNT=$2
	IFS='<'
	set $COUNT
	COUNT=$1	
	let COUNT++
	echo $COUNT
	
	sed -i 's/<notecount>.*<\/notecount>/<notecount>'$COUNT'<\/notecount>/ '$DIR$TEMPG
	sed -i 's/<modificationdate>.*<\/modificationdate>/<modificationdate>'$DATE'<\/modificationdate>/' $DIR$TEMPG
	
	echo '<note>' > $DIR$TEMP
	echo '		<id>'$COUNT'</id>' >> $DIR$TEMP
	echo '		<creationdate>'$DATE'</creationdate>' >> $DIR$TEMP
        echo '	        <author>'$BLAZE'</author>' >> $DIR$TEMP
	echo '		<contenu>'$CONTENT'</contenu>' >> $DIR$TEMP
	
	if [ $ATRIMAGE = -img ]  
	then
	    sendImg $IMAGE $DIRIMAGE$FILE'/'$DATE $FILE
	fi

	echo '	    </note>' >> $DIR$TEMP
	echo '	</notes>' >> $DIR$TEMP
	echo '</item>' >> $DIR$TEMP
	paste -z $DIR$TEMPG $DIR$TEMP > $DIR$TEMPF
	head -n -1 $DIR$TEMPF > $DIR'xml/'$FILE$FMT
	sendLog $FILE

    fi

}

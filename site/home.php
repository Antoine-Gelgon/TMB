<html>
<head lang="fr-FR">
    <meta charset="UTF-8">
    <link rel="stylesheet" href="css/style.css" type="text/css">
    <script type="text/javascript" src="js/jquery-1.12.1.min.js"></script>
</head>
    <body>
      <div class="page" >

   <header><a>Antoine Gelgon<span> Comment affecter un style à une balise span, obtenir un effet avec CSS et être compatible avec une majorité de navigateurs, pas de javascript</span></a> - Type & Graphic Design - FreeSoftware - Brussels - <a href="http://www.atelier-bek.be/" >Atelier Bek</a> - <a id="btn-links" data-gaba="links" data-log="links" >Links</a> 

<?php
   $dir = 'xml/';
   if (is_dir($dir)) {
       if ($dh = opendir($dir)) {
	   while (($file = readdir($dh)) !== false) {
	     if( $file != '.' && $file != '..' && preg_match('#\.(xml)$#i', $file)) {
	       $file = explode(".", $file);
	       $xml_list = simplexml_load_file('xml/'.$file[0].'.xml');
	       $titre = $xml_list->meta->titre;
	       $tag_list = $xml_list->meta->tags;
	       $online = $xml_list->meta->online;
	       $lastImg = $xml_list->notes->showcase->image;

	       //Ici on récupère la dernière image du dossier;

	       if( $online == 'on'){
?>
		  <a class="btn-projet" data-img="images/<?php echo $lastImg; ?>" data-gaba="projet" data-log="<?php echo $file[0];?>" >! <?php echo $titre; ?> <span> <?php echo $tag_list; ?></span></a>
<?php
	       }
	    }
        }
        closedir($dh);
    }
}

?>
   </header>

   <div class="thumbnail"></div>
    <div class="contenu" >
   </div>
  </div>

  <script type="text/javascript">
    $(document).ready(function(){
      loadCont('.btn-projet', '.contenu');
      loadCont('#btn-links', '.contenu');
      resizePage();
    });
      // $.ajaxSetup({cache:false});
  </script>

<div class="cont_links" 
   <?php $theTitle = 'Antoine Gelgon - Links'; ?>
  
      <?php
        ///////////////////////////
        // Here goes your feeds: //
        ///////////////////////////
        $file = "http://antoine-gelgon.fr/shaarli/?do=rss";
        $content = file_get_contents($file);
        $x = new SimpleXmlElement($content);
        echo "<ul>";

        foreach($x->channel->item as $entry) {
          echo "<li>";
	  echo "  <a class='title' target='_blank' href='".$entry->link."' >" . $entry->title . "</a>
                  <span class='toggleInfos'> ---> ";
          echo "    <span class='description'> " . $entry->description . "</span>";
          $categories = $entry->category;
            $nb = count($categories);
            for ($i=0; $i<$nb; $i++){
              echo '<span class="'.$entry->title.' categories">' .$categories[$i]. '</span> ';
            }
          echo " </span>";
          echo "</li>";
        }
        echo "</ul>";

      ?>
    </div>
  <script>
    function showtitle(more, less, title){
      more.click(function(){
        title.show();
        less.show();
        more.hide();
      })
      less.click(function(){
        title.hide();
        less.hide();
        more.show();
      })
    };
  showtitle($('.moreInfos'), $('.lessInfos'), $('.toggleInfos'));
  showtitle($('.moreCredits'), $('.lessCredits'), $('.footer'));
  </script>
</div>

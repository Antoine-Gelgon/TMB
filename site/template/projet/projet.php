<?php
include 'replace.php';
   $text=$_GET['log'];
   $xml = simplexml_load_file('../../xml/'.$text.'.xml');
   $titre = $xml->meta->titre;
   $description = $xml->meta->description;
   $tags = $xml->meta->tags;
   $count = $xml->meta->notecount;
   $git = $xml->meta->git;
?>

   <div class="colMeta" >
       <span> <?php echo $titre; ?></span> 
       <span> <?php echo $description; ?></span>
       <span> [<?php echo $tags; ?>]</span>

      <?php if ($git) {
	$url_git = explode('/', $git);
	$user_git = $url_git[3];
	$repo_git = $url_git[4];
	       echo '<a class="btn-git" data-gaba="git" data-log="'. $text .'/'. $user_git .'/'. $repo_git .'" >-> CHECK THE GIT !! <-</a>';
	}
      ?>
      <div class='colLog'>
	 <a class="title">-> CHANGELOG !!<-</a>
	 <div class="logNotes">
	    <?php  
	       $id=0;

	       //LOG
	       $reverseArray = (array) $xml->notes;
	       $reverseArray = array_reverse($reverseArray["note"]);
	       foreach($reverseArray as $pas){
		   $contenu = $pas->contenu;
		   $image = $pas->image;
		   $id++;
		   $author = $pas->author;
		   $datepost = $pas->creationdate;
		   $enter = array('[c]','[/c]','[l]','[/l]',);
		   $exit = array('<code>','</code>','<a>','</a>');
		   $contenu = preg_replace('/\((.*?)\)/is', '<span class="menus" >$0</span>', $contenu);
		   $contenu = str_replace($enter, $exit, $contenu);
		   $contenu = preg_replace('#http://[a-z0-9._/-]+#i', '<a TARGET="_BLANK" href="$0">$0</a>', $contenu);
		   $contenu = preg_replace('#https://[a-z0-9._/-]+#i', '<a TARGET="_BLANK" href="$0">$0</a>', $contenu);
		   echo '<div class="note" id="log_'.$id.'" >';
		   echo '<div class="date" ><span>'.$datepost.'</span><span> -- ></span></div>';
		   echo '<div class="text" >'.$contenu.'</div>';
		   if(!empty($image)){
		       echo '<br><img class="imgNote" src="images/'.$image.'" />';
		   }
		   echo '</div><br>';
	       }
	    ?>
	 </div>
      </div>

   </div>




<div class="zoomLog"></div>
<div class="colShowcase" >
<?php
   $reverseArray = (array) $xml->notes;
   $reverseArray = array_reverse($reverseArray["showcase"]);
?>
   <div class="slideroff">
<?php
   $i_Slide=0;
   foreach($reverseArray as $pas){
      $image = $pas->image;
?>
      <img class="<?php echo $i_Slide; ?>" src="images/<?php echo $image; ?>" />

<?php
      $i_Slide++;
   }
?>
   </div>
</div>
   <script type='text/javascript'>
   $('.zoomLog').hide(); 

   $('.Note').mouseover(function(){
      $('.zoomLog').empty(); 
      $('.zoomLog').stop().fadeIn(500); 
      var text = $(this).html();
      $('.zoomLog').html(text); 
   });

   $('.Note').mouseleave(function(){
      $('.zoomLog').stop().fadeOut(500);
   });

    $(document).ready(function(){
      loadCont('.btn-git', '.contenu');
    });
      // $.ajaxSetup({cache:false});
</script>

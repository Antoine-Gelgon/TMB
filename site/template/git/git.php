<?php


$info_git = explode('/', $_GET['log']);
$user_git = $info_git[1];
$repo_git = $info_git[2];
$projet_name = $info_git[0];
$url_git = 'https://gitlab.com'; 
require('../../includes/simple_html_dom.php');

$html = new simple_html_dom();
$html->load_file( $url_git .'/'. $user_git .'/'. $repo_git .'/tree/master');
$readme = $html->find('.file-content', 0);
$readme = str_replace('src="/'. $user_git .'/', 'src="'. $url_git .'/'. $user_git .'/', $readme);
?>
  <div class="head_git" title="<?= $user_git ?>" data-projet="<?= $repo_git ?>" >
    <a class="btn-back" data-log="<?= $projet_name ?>" data-gaba="single" style="color: darkred;" ><<<<<--</a> <?= $projet_name ?>
  </div>
  <div class="repo_list" title='<?= $user_git ?>' data-projet='<?= $repo_git ?>' > 
    <?php
      $num =  0;
      foreach ($html->find('.tree-item-file-name') as $element){
	  // $element = str_replace('&#32;', '/%/', $element);
	echo '<div class="range-list" title="'.$user_git .'" data-projet="'. $repo_git .'"  >'.$element.'</div>';
	$num++;
};
    ?>
</div>
<div class="repo_viewer"> 
<?php echo $readme; ?>
</div>
<?php


?>

<script>
  loadCont('.btn-back','.contenu');
  git_style('.repo_list');
</script>

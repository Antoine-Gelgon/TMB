var tableFormat = [];
    // FONT 
    tableFormat['ttf']   = ['TrueType','font'];
    tableFormat['otf']   = ['OpenType', 'font'];
    //CODE
    tableFormat['php']   = ['PHP', 'code'];
    tableFormat['html']  = ['HTML', 'code'];
    tableFormat['css']   = ['CSS', 'code'];
    tableFormat['less']  = ['LESS', 'code'];
    tableFormat['js']    = ['JavaScript', 'code'];
    tableFormat['sh']    = ['Shell', 'code'];
    //TEXT
    tableFormat['txt']   = ['Text','text'];
    tableFormat['md']    = ['Markdown', 'text'];
    //DOWNLOAD
    tableFormat['zip']   = ['ZIP', 'download'];
    //IMAGES
    tableFormat['jpg']   = ['JPG', 'image'];
    tableFormat['png']   = ['PNG', 'image'];

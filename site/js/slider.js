function slider(){
     var slider  = $('.slider');
     var total = slider.length;
     var nb = 0;
     
     function PosImg(valClass){
	var wImg = slider.children('.'+valClass).width();
	var hImg = slider.children('.'+valClass).height();
	if( hImg > wImg){
	    slider.children('.'+valClass).height('98%');
	}
     }

     slider.children().hide();
     slider.children('.0').show();
     PosImg(0);
     slider.children().click(function(){
	   $('.'+nb).hide();
	   if(nb == total+1){
	     nb = 0;
	   } else {
	     nb=nb+1;
	   }
	    slider.children('.'+nb).show();
	   PosImg(nb);
      });
}

slider();
